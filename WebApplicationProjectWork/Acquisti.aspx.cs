﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProjectWork.Classes;

namespace WebApplicationProjectWork
{
    public partial class Acquisti : System.Web.UI.Page
    {
        private String miaPassword = "****";















































        protected void Page_Load(object sender, EventArgs e)
        {
            this.Aggiungi.Click += Aggiungi_Click;
            if (!this.IsPostBack)
            {
                getAll();
                getFornitori();
                getArticoli();
                getCarrello();
            }    
            this.GridViewArticoli.RowCommand += GridViewArticoli_RowCommand;
            this.GridViewCarrello.RowCommand += GridViewCarrello_RowCommand;
            this.GridView.RowCommand += GridView_RowCommand;
        }

        private void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Carico")
            {
                SqlConnection mySqlConnection = null;
                try
                {
                    String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    mySqlConnection = new SqlConnection(stringaConnessione);
                    SqlCommand mySqlCommand = new SqlCommand();
                    mySqlCommand.Connection = mySqlConnection;
                    mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa

                    Int32 index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow dv = GridView.Rows[index];

                    Int32 acquistoID = Convert.ToInt32(dv.Cells[0].Text);
                    mySqlCommand.Parameters.Add("@AcquistoID", SqlDbType.Int);
                    mySqlCommand.Parameters["@AcquistoID"].Value = acquistoID;
                    mySqlCommand.CommandText = "SELECT * FROM Acquisti WHERE AcquistoID = @AcquistoID";
                    mySqlConnection.Open();
                    SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                    if (mySqlDataReader.Read())
                    {
                        Acquisto acquisto = new Acquisto()
                        {
                            AcquistoID = Convert.ToInt32(mySqlDataReader["AcquistoID"]),
                            FornitoreID = Convert.ToInt32(mySqlDataReader["FornitoreID"]),
                            NumeroFattura = mySqlDataReader["NumeroFattura"].ToString(),
                            DataFattura = mySqlDataReader["DataFattura"].ToString(),
                            CaricoEffettuato = Convert.ToInt32(mySqlDataReader["CaricoEffettuato"])
                        };
                          
                        if (acquisto.CaricoEffettuato == 0)
                        {
                            mySqlConnection.Close();
                            mySqlConnection.Open();
                            mySqlCommand.Parameters.Add("@CaricoEffettuato", SqlDbType.Int);
                            mySqlCommand.Parameters["@CaricoEffettuato"].Value = dv.Cells[0].Text;
                            mySqlCommand.CommandText = "UPDATE Acquisti SET CaricoEffettuato = 1 WHERE AcquistoID = @AcquistoID";
                            mySqlCommand.ExecuteNonQuery();
                            Messaggio.ForeColor = Color.Green;
                            Messaggio.Text = "Carico effettuato";
                        } 
                        else
                        {
                            Messaggio.ForeColor = Color.Red;
                            Messaggio.Text = "Carico già effettuato";
                        } 
                        
                    }
                    else
                    {
                        Messaggio.Text = "Non sono presenti articoli";
                    }
                }
                catch (Exception exc)
                {
                    Messaggio.ForeColor = Color.Red;
                    Messaggio.Text = "Errore: " + exc.Message;
                }
                finally
                {
                    mySqlConnection.Close();
                }
            }
        }

        private void GridViewCarrello_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Rimuovi")
            {
                Int32 index = Convert.ToInt32(e.CommandArgument);
                GridViewRow dv = GridViewCarrello.Rows[index];
                dv.Cells[6].Text = Convert.ToString(Convert.ToInt32(dv.Cells[6].Text) - 1);
                if (dv.Cells[6].Text == "0")
                {
                    DataTable dt = GetDataTable(GridViewCarrello);
                    dt.Rows[index].Delete();
                    ViewState["dt"] = dt;
                    BindGrid();
                }
                /*Int32 giacenza = Convert.ToInt32(GridViewArticoli.Rows[index].Cells[5].Text) + 1;
                GridViewArticoli.Rows[index].Cells[5].Text = giacenza.ToString();*/
            }
        }

        private void BindGrid()
        {
            GridViewCarrello.DataSource = ViewState["dt"] as DataTable;
            GridViewCarrello.DataBind();
        }

        private void getCarrello()
        {
            // primo caricamento -> codice che deve essere eseguito una sola volta, al caricamento della pagina
            this.GridViewCarrello.AutoGenerateColumns = false;
            // spetta a noi definire i vari campi della grid
            BoundField myBoundFieldItemID = new BoundField
            {
                DataField = "ItemID",
                HeaderText = "ItemID"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldItemID);
            BoundField myBoundFieldASIN = new BoundField
            {
                DataField = "ASIN",
                HeaderText = "ASIN"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldASIN);
            BoundField myBoundFieldTitle = new BoundField
            {
                DataField = "Title",
                HeaderText = "Title"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldTitle);
            BoundField myBoundFieldCategoria = new BoundField
            {
                DataField = "Categoria",
                HeaderText = "Categoria"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldCategoria);
            BoundField myBoundFieldPrezzo = new BoundField
            {
                DataField = "Prezzo",
                HeaderText = "Prezzo"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldPrezzo);
            BoundField myBoundFieldBrand = new BoundField
            {
                DataField = "Brand",
                HeaderText = "Brand"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldBrand);
            BoundField myBoundFieldQuantità = new BoundField
            {
                DataField = "Quantita",
                HeaderText = "Quantita"
            };
            this.GridViewCarrello.Columns.Add(myBoundFieldQuantità);
            ButtonField myButtonFieldRimuovi = new ButtonField
            {
                ButtonType = ButtonType.Button,
                Text = "Rimuovi un articolo",
                CommandName = "Rimuovi"
            };
            this.GridViewCarrello.Columns.Add(myButtonFieldRimuovi);
        }

        private void GridViewArticoli_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Aggiungi")
            {
                Int32 index = Convert.ToInt32(e.CommandArgument);
                GridViewRow dv = GridViewArticoli.Rows[index];
                Articolo articoloSelezionato = new Articolo()
                {
                    ItemID = Convert.ToInt32(dv.Cells[0].Text),
                    ASIN = Convert.ToString(dv.Cells[1].Text),
                    Title = Convert.ToString(dv.Cells[2].Text),
                    Categoria = Convert.ToString(dv.Cells[3].Text),
                    Prezzo = Convert.ToDouble(dv.Cells[4].Text),
                    Giacenza = Convert.ToInt32(dv.Cells[5].Text),
                    Brand = Convert.ToString(dv.Cells[6].Text)
                };
                DataTable dt = GetDataTable(GridViewCarrello);
                Boolean toAdd = true;
                Int32 quantità = 1;

                if (dt.Rows.Count == 0)
                {
                    dt = new DataTable();
                    dt.Columns.Add("ItemID");
                    dt.Columns.Add("ASIN");
                    dt.Columns.Add("Title");
                    dt.Columns.Add("Categoria");
                    dt.Columns.Add("Prezzo");
                    dt.Columns.Add("Brand");
                    dt.Columns.Add("Quantita");
                } 
                else
                {
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        if (Convert.ToString(dataRow["ASIN"]) == articoloSelezionato.ASIN)
                        {
                            dataRow["Quantita"] = Convert.ToInt32(dataRow["Quantita"]) + 1;
                            toAdd = false;
                            break;
                        }
                    }
                }

                if (toAdd)
                {
                    DataRow dr = dt.NewRow();
                    dr["ItemID"] = articoloSelezionato.ItemID;
                    dr["ASIN"] = articoloSelezionato.ASIN;
                    dr["Title"] = articoloSelezionato.Title;
                    dr["Categoria"] = articoloSelezionato.Categoria;
                    dr["Prezzo"] = articoloSelezionato.Prezzo;
                    dr["Brand"] = articoloSelezionato.Brand;
                    dr["Quantita"] = quantità;

                    dt.Rows.Add(dr);
                }
               
                GridViewCarrello.DataSource = dt;
                GridViewCarrello.DataBind();

                /*Int32 giacenza = Convert.ToInt32(GridViewArticoli.Rows[index].Cells[5].Text) - 1;
                GridViewArticoli.Rows[index].Cells[5].Text = giacenza.ToString();*/
            }
        }

        private void getArticoli()
        {
            // primo caricamento -> codice che deve essere eseguito una sola volta, al caricamento della pagina
            this.GridViewArticoli.AutoGenerateColumns = false;
            // spetta a noi definire i vari campi della grid
            BoundField myBoundFieldItemID = new BoundField
            {
                DataField = "ItemID",
                HeaderText = "ItemID"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldItemID);
            BoundField myBoundFieldArticolo = new BoundField
            {
                DataField = "ASIN",
                HeaderText = "ASIN"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldArticolo);
            BoundField myBoundFieldNome = new BoundField
            {
                DataField = "Title",
                HeaderText = "Title"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldNome);
            BoundField myBoundFieldDescrizione = new BoundField
            {
                DataField = "Categoria",
                HeaderText = "Categoria"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldDescrizione);
            BoundField myBoundFieldPrezzo = new BoundField
            {
                DataField = "Prezzo",
                HeaderText = "Prezzo"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldPrezzo);
            BoundField myBoundFieldGiacenza = new BoundField
            {
                DataField = "Giacenza",
                HeaderText = "Giacenza"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldGiacenza);
            BoundField myBoundFieldAliquota = new BoundField
            {
                DataField = "Brand",
                HeaderText = "Brand"
            };
            this.GridViewArticoli.Columns.Add(myBoundFieldAliquota);
            ButtonField myButtonFieldModifica = new ButtonField
            {
                ButtonType = ButtonType.Button,
                Text = "Aggiungi al carrello",
                CommandName = "Aggiungi"
            };
            this.GridViewArticoli.Columns.Add(myButtonFieldModifica);

            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                mySqlCommand.CommandText = "SELECT * FROM Items";
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                if (mySqlDataReader.Read())
                {
                    List<Articolo> listArticoli = new List<Articolo>();
                    while (mySqlDataReader.Read())
                    {
                        Articolo articolo = new Articolo()
                        {
                            ASIN = Convert.ToString(mySqlDataReader["ASIN"]),
                            ItemID = Convert.ToInt32(mySqlDataReader["ItemID"]),
                            Title = Convert.ToString(mySqlDataReader["Title"]),
                            Categoria = Convert.ToString(mySqlDataReader["Categoria"]),
                            Giacenza = Convert.ToInt32(mySqlDataReader["Giacenza"]),
                            Prezzo = Convert.ToDouble(mySqlDataReader["Prezzo"]),
                            Brand = Convert.ToString(mySqlDataReader["Brand"]),
                        };
                        listArticoli.Add(articolo);
                    }
                    this.GridViewArticoli.DataSource = listArticoli;
                    this.GridViewArticoli.DataBind();
                }
                else
                {
                    Messaggio.Text = "Non sono presenti articoli";
                }
            }
            catch (Exception exc)
            {
                Messaggio.ForeColor = Color.Red;
                Messaggio.Text = "Non è stato possibile caricare gli articoli a causa di un errore: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        private void getFornitori()
        {
            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                mySqlCommand.CommandText = "SELECT * FROM Fornitori";
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                List<Fornitore> list = new List<Fornitore>();
                while (mySqlDataReader.Read())
                {
                    Fornitore f = new Fornitore()
                    {
                        FornitoreID = Convert.ToInt32(mySqlDataReader["FornitoreID"]),
                        Nome = Convert.ToString(mySqlDataReader["Nome"])
                    };
                    list.Add(f);
                }
                listaFornitori.DataSource = list;
                listaFornitori.DataTextField = "Nome";
                listaFornitori.DataValueField = "FornitoreID";
                listaFornitori.DataBind();
            }
            catch (Exception exc)
            {

            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                mySqlConnection.Open();
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text;

                mySqlCommand.Parameters.Add("@FornitoreID", SqlDbType.Int);
                mySqlCommand.Parameters["@FornitoreID"].Value = listaFornitori.SelectedValue;
                mySqlCommand.Parameters.Add("@DataFattura", SqlDbType.Date);
                mySqlCommand.Parameters["@DataFattura"].Value = dataFattura.Text;
                mySqlCommand.Parameters.Add("@NumeroFattura", SqlDbType.NVarChar);
                mySqlCommand.Parameters["@NumeroFattura"].Value = numeroFattura.Text;

                mySqlCommand.CommandText = "INSERT INTO Acquisti(FornitoreID, DataFattura, NumeroFattura) " +
                                           "VALUES (@FornitoreID, @DataFattura, @NumeroFattura)";
                mySqlCommand.ExecuteNonQuery();

                mySqlConnection.Close();
                mySqlConnection.Open();

                SqlCommand mySqlCommand2 = new SqlCommand();
                mySqlCommand2.Connection = mySqlConnection;
                mySqlCommand2.CommandType = System.Data.CommandType.Text;
                mySqlCommand2.Parameters.Add("@FornitoreID", SqlDbType.Int);
                mySqlCommand2.Parameters["@FornitoreID"].Value = listaFornitori.SelectedValue;
                mySqlCommand2.Parameters.Add("@DataFattura", SqlDbType.Date);
                mySqlCommand2.Parameters["@DataFattura"].Value = dataFattura.Text;
                mySqlCommand2.Parameters.Add("@NumeroFattura", SqlDbType.NVarChar);
                mySqlCommand2.Parameters["@NumeroFattura"].Value = numeroFattura.Text;
                mySqlCommand2.CommandText = "SELECT AcquistoID FROM Acquisti " +
                                           "WHERE FornitoreID = @FornitoreID AND DataFattura = @DataFattura" +
                                           "    AND NumeroFattura = @NumeroFattura";
                SqlDataReader mySqlDataReader = mySqlCommand2.ExecuteReader();

                Int32 acquistoID = 0;
                while (mySqlDataReader.Read())
                {
                    acquistoID = Convert.ToInt32(mySqlDataReader["AcquistoID"]);
                }

                mySqlConnection.Close();
                mySqlConnection.Open();

                foreach (GridViewRow gridViewRow in GridViewCarrello.Rows)
                {
                    SqlCommand mySqlCommand3 = new SqlCommand();
                    mySqlCommand3.Connection = mySqlConnection;
                    mySqlCommand3.CommandType = System.Data.CommandType.Text;
                    mySqlCommand3.Parameters.Add("@AcquistoID", SqlDbType.Int);
                    mySqlCommand3.Parameters["@AcquistoID"].Value = acquistoID;
                    mySqlCommand3.Parameters.Add("@ItemID", SqlDbType.Int);
                    mySqlCommand3.Parameters["@ItemID"].Value = gridViewRow.Cells[0].Text;
                    mySqlCommand3.Parameters.Add("@QuantitàAcquistata", SqlDbType.Int);
                    mySqlCommand3.Parameters["@QuantitàAcquistata"].Value = gridViewRow.Cells[6].Text;
                    mySqlCommand3.Parameters.Add("@PrezzoUnitarioAcquisto", SqlDbType.Float);
                    mySqlCommand3.Parameters["@PrezzoUnitarioAcquisto"].Value = gridViewRow.Cells[4].Text;

                    mySqlCommand3.CommandText = "INSERT INTO AcquistiItems(AcquistoID, ItemID, QuantitàAcquistata, PrezzoUnitarioAcquisto) " +
                                           "VALUES (@AcquistoID, @ItemID, @QuantitàAcquistata, @PrezzoUnitarioAcquisto)";
                    mySqlCommand3.ExecuteNonQuery();

                    mySqlConnection.Close();
                    mySqlConnection.Open();

                    DataTable dt = GetDataTable(GridViewArticoli);
                    DataRow[] foundRows = dt.Select("ItemID = " + gridViewRow.Cells[0].Text);
                    DataRow myRow = foundRows[0];

                    SqlCommand mySqlCommand4 = new SqlCommand();
                    mySqlCommand4.Connection = mySqlConnection;
                    mySqlCommand4.CommandType = System.Data.CommandType.Text;
                    mySqlCommand4.Parameters.Add("@ItemID", SqlDbType.Int);
                    mySqlCommand4.Parameters["@ItemID"].Value = gridViewRow.Cells[0].Text;
                    mySqlCommand4.Parameters.Add("@Giacenza", SqlDbType.Int);
                    mySqlCommand4.Parameters["@Giacenza"].Value = Convert.ToInt32(myRow["Giacenza"]) - Convert.ToInt32(gridViewRow.Cells[6].Text);
                    mySqlCommand4.CommandText = "UPDATE Items SET Giacenza = @Giacenza WHERE ItemID = @ItemID";
                    mySqlCommand4.ExecuteNonQuery();

                    mySqlConnection.Close();
                    mySqlConnection.Open();
                }

                this.MessaggioInsert.Text = "Acquisto Inserito";
            }
            catch (Exception exc)
            {
                this.MessaggioInsert.Text = "errore nell'inserimento dell'acquisto: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        private void getAll()
        {
            this.GridView.AutoGenerateColumns = false;
            // spetta a noi definire i vari campi della grid
            BoundField myBoundFieldAcquistoID = new BoundField
            {
                DataField = "AcquistoID",
                HeaderText = "AcquistoID"
            };
            this.GridView.Columns.Add(myBoundFieldAcquistoID);
            BoundField myBoundFieldNome = new BoundField
            {
                DataField = "Nome",
                HeaderText = "Nome"
            };
            this.GridView.Columns.Add(myBoundFieldNome);
            BoundField myBoundFieldDataFattura = new BoundField
            {
                DataField = "DataFattura",
                HeaderText = "DataFattura"
            };
            this.GridView.Columns.Add(myBoundFieldDataFattura);
            BoundField myBoundFieldNumeroFattura = new BoundField
            {
                DataField = "NumeroFattura",
                HeaderText = "NumeroFattura"
            };
            this.GridView.Columns.Add(myBoundFieldNumeroFattura);
            ButtonField myButtonFieldCarico = new ButtonField
            {
                ButtonType = ButtonType.Button,
                Text = "Effettua carico magazzino",
                CommandName = "Carico"
            };
            this.GridView.Columns.Add(myButtonFieldCarico);

            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                mySqlCommand.CommandText = "SELECT A.AcquistoID, F.Nome, A.DataFattura, A.NumeroFattura FROM Acquisti A INNER JOIN Fornitori F " +
                                           "ON A.FornitoreID = F.FornitoreID";
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                if (mySqlDataReader.Read())
                {
                    GridView.DataSource = mySqlDataReader;
                    GridView.DataBind();
                }
                else
                {
                    Messaggio.Text = "Non sono presenti articoli";
                }
            }
            catch (Exception exc)
            {
                Messaggio.ForeColor = Color.Red;
                Messaggio.Text = "Non è stato possibile caricare gli articoli a causa di un errore: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        DataTable GetDataTable(GridView dtg)
        {
            DataTable dt = new DataTable();

            // add the columns to the datatable            
            if (dtg.HeaderRow != null)
            {

                for (int i = 0; i < dtg.HeaderRow.Cells.Count; i++)
                {
                    if (!dt.Columns.Contains(dtg.HeaderRow.Cells[i].Text)) {
                        dt.Columns.Add(dtg.HeaderRow.Cells[i].Text);
                    }
                }
            }

            //  add each of the data rows to the table
            foreach (GridViewRow row in dtg.Rows)
            {
                DataRow dr;
                dr = dt.NewRow();

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dr[i] = Convert.ToString(row.Cells[i].Text);
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }
}