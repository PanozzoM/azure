﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProjectWork.Classes;

namespace WebApplicationProjectWork
{
    public partial class Vendite : System.Web.UI.Page
    {
        private String miaPassword = "****";
















































        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                getAll();
            }
        }

        private void getAll()
        {
            HttpClient httpClient = new HttpClient();
            var responseTask = httpClient.GetAsync("https://projectwork.gomulgame.com/WebServiceOrders.asmx/orders?refresh_token=Atzr|IwEBIPGGbogA4gJ86OciHsp16r6gXmV&CreatedAfter=2021-06-01T16:09:52.000&CreatedBefore=2021-07-31T16:09:52.000");
            responseTask.Wait();
            Ordini listOrdini = new Ordini(); ;
            var content = responseTask.Result.Content.ReadAsStringAsync();
            listOrdini = JsonConvert.DeserializeObject<Ordini>(content.Result);

            List<Ordine> orders = new List<Ordine>();
            GridView.DataSource = orders;
            GridView.DataBind();

            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                mySqlCommand.CommandText = "SELECT AmazonOrderId FROM Orders";
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                List<Int32> ordersAlreadyPresent = new List<Int32>();
                while (mySqlDataReader.Read())
                {
                    ordersAlreadyPresent.Add(Convert.ToInt32(mySqlDataReader["AmazonOrderId"]));
                }

                foreach (Ordine order in listOrdini.ordini)
                {
                    if (!ordersAlreadyPresent.Contains(Convert.ToInt32(order.AmazonOrderId.ToString().Replace("-", ""))))
                    {
                        mySqlConnection.Close();
                        mySqlConnection.Open();

                        mySqlCommand.Parameters.Add("@AmazonOrderId", SqlDbType.Int);
                        mySqlCommand.Parameters["@AmazonOrderId"].Value = order.AmazonOrderId;
                        mySqlCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@BuyerEmail"].Value = order.BuyerEmail;
                        mySqlCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@BuyerName"].Value = order.BuyerName;
                        mySqlCommand.Parameters.Add("@CompanyLegalName", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@CompanyLegalName"].Value = order.CompanyLegalName;
                        mySqlCommand.Parameters.Add("@EarliestShipDate", SqlDbType.Date);
                        mySqlCommand.Parameters["@EarliestShipDate"].Value = order.EarliestShipDate;
                        mySqlCommand.Parameters.Add("@FulfillmentChannel", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@FulfillmentChannel"].Value = order.FulfillmentChannel;
                        mySqlCommand.Parameters.Add("@IsBusinessOrder", SqlDbType.Bit);
                        mySqlCommand.Parameters["@IsBusinessOrder"].Value = order.IsBusinessOrder;
                        mySqlCommand.Parameters.Add("@IsGlobalExpressEnabled", SqlDbType.Bit);
                        mySqlCommand.Parameters["@IsGlobalExpressEnabled"].Value = order.IsGlobalExpressEnabled;
                        mySqlCommand.Parameters.Add("@IsPremiumOrder", SqlDbType.Bit);
                        mySqlCommand.Parameters["@IsPremiumOrder"].Value = order.IsPremiumOrder;
                        mySqlCommand.Parameters.Add("@IsPrime", SqlDbType.Bit);
                        mySqlCommand.Parameters["@IsPrime"].Value = order.IsPrime;
                        mySqlCommand.Parameters.Add("@IsSoldByAB", SqlDbType.Bit);
                        mySqlCommand.Parameters["@IsSoldByAB"].Value = order.IsSoldByAB;
                        mySqlCommand.Parameters.Add("@LastUpdateDate", SqlDbType.Date);
                        mySqlCommand.Parameters["@LastUpdateDate"].Value = order.LastUpdateDate;
                        mySqlCommand.Parameters.Add("@LatestShipDate", SqlDbType.Date);
                        mySqlCommand.Parameters["@LatestShipDate"].Value = order.LatestShipDate;
                        mySqlCommand.Parameters.Add("@MarketplaceId", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@MarketplaceId"].Value = order.MarketplaceId;
                        mySqlCommand.Parameters.Add("@NumberOfItemsShipped", SqlDbType.Int);
                        mySqlCommand.Parameters["@NumberOfItemsShipped"].Value = order.NumberOfItemsShipped;
                        mySqlCommand.Parameters.Add("@NumberOfItemsUnshipped", SqlDbType.Int);
                        mySqlCommand.Parameters["@NumberOfItemsUnshipped"].Value = order.NumberOfItemsUnshipped;
                        mySqlCommand.Parameters.Add("@OrderStatus", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@OrderStatus"].Value = order.OrderStatus;
                        mySqlCommand.Parameters.Add("@OrderType", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@OrderType"].Value = order.OrderType;
                        mySqlCommand.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@PaymentMethod"].Value = order.PaymentMethod;
                        mySqlCommand.Parameters.Add("@PaymentMethodDetails", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@PaymentMethodDetails"].Value = order.PaymentMethodDetails;
                        mySqlCommand.Parameters.Add("@PurchaseDate", SqlDbType.Date);
                        mySqlCommand.Parameters["@PurchaseDate"].Value = order.PurchaseDate;
                        mySqlCommand.Parameters.Add("@PurchaseOrderNumber", SqlDbType.Int);
                        mySqlCommand.Parameters["@PurchaseOrderNumber"].Value = order.PurchaseOrderNumber;
                        mySqlCommand.Parameters.Add("@ShipmentServiceLevelCategory", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShipmentServiceLevelCategory"].Value = order.ShipmentServiceLevelCategory;
                        mySqlCommand.Parameters.Add("@ShippingAddressCity", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShippingAddressCity"].Value = order.ShippingAddressCity;
                        mySqlCommand.Parameters.Add("@ShippingAddressLine1", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShippingAddressLine1"].Value = order.ShippingAddressLine1;
                        mySqlCommand.Parameters.Add("@ShippingAddressName", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShippingAddressName"].Value = order.ShippingAddressName;
                        mySqlCommand.Parameters.Add("@ShippingCityStateOrRegion", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShippingCityStateOrRegion"].Value = order.ShippingCityStateOrRegion;
                        mySqlCommand.Parameters.Add("@ShippingStateOrRegionPostalCode", SqlDbType.NVarChar);
                        mySqlCommand.Parameters["@ShippingStateOrRegionPostalCode"].Value = order.ShippingStateOrRegionPostalCode;

                        mySqlCommand.CommandText = "INSERT INTO Orders (AmazonOrderId, BuyerEmail, BuyerName, CompanyLegalName, EarliestShipDate, FulfillmentChannel, IsBusinessOrder, IsGlobalExpressEnabled, " +
                                                   "IsPremiumOrder, IsPrime, IsSoldByAB, LastUpdateDate, LatestShipDate, MarketplaceId, NumberOfItemsShipped, NumberOfItemsUnshipped, OrderStatus, " +
                                                   "OrderType, PaymentMethod, PaymentMethodDetails, PurchaseDate, PurchaseOrderNumber, ShipmentServiceLevelCategory, ShippingAddressCity, ShippingAddressLine1, " +
                                                   "ShippingAddressName, ShippingCityStateOrRegion, ShippingStateOrRegionPostalCode) " +
                                                   "VALUES (@AmazonOrderId, @BuyerEmail, @BuyerName, @CompanyLegalName, @EarliestShipDate, @FulfillmentChannel, @IsBusinessOrder, @IsGlobalExpressEnabled, " +
                                                   "@IsPremiumOrder, @IsPrime, @IsSoldByAB, @LastUpdateDate, @LatestShipDate, @MarketplaceId, @NumberOfItemsShipped, @NumberOfItemsUnshipped, @OrderStatus, " +
                                                   "@OrderType, @PaymentMethod, @PaymentMethodDetails, @PurchaseDate, @PurchaseOrderNumber, @ShipmentServiceLevelCategory, @ShippingAddressCity, @ShippingAddressLine1, " +
                                                   "@ShippingAddressName, @ShippingCityStateOrRegion, @ShippingStateOrRegionPostalCode)";
                        mySqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exc)
            {
                Messaggio.BackColor = Color.Red;
                Messaggio.Text = "Errore: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }
    }
}