﻿<%@ Page Title="Articoli" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Articoli.aspx.cs" Inherits="WebApplicationProjectWork.Articoli" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Articoli</h1>
    <br />
    <asp:Label ID="Messaggio" runat="server" Text=""></asp:Label>
    <asp:GridView ID="GridView" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <br />
    <table style="width: 30%;">
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelASIN" runat="server" Text="ASIN: "></asp:Label></td>
            <td><asp:TextBox ID="ASIN" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelTitle" runat="server" Text="Title: "></asp:Label></td>
            <td><asp:TextBox ID="title" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelCategoria" runat="server" Text="Categoria: "></asp:Label></td>
            <td><asp:TextBox ID="Categoria" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelPrezzo" runat="server" Text="Prezzo: "></asp:Label></td>
            <td><asp:TextBox ID="Prezzo" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelGiacenza" runat="server" Text="Giacenza: "></asp:Label></td>
            <td><asp:TextBox ID="Giacenza" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelBrand" runat="server" Text="Brand: "></asp:Label></td>
            <td><asp:TextBox ID="Brand" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2"><asp:Button ID="Aggiungi" runat="server" Text="Aggiungi" class="btn btn-primary" Width="129px"/></td>
        </tr>
    </table>
    <asp:Label ID="MessaggioInsert" runat="server" Text=""></asp:Label>
</asp:Content>
