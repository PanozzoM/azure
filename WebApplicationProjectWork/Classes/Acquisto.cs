﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProjectWork.Classes
{
    public class Acquisto
    {
        public Int32 AcquistoID { get; set; }
        public Int32 FornitoreID { get; set; }
        public String DataFattura { get; set; }
        public String NumeroFattura { get; set; }
        public Int32 CaricoEffettuato { get; set; }
    }
}