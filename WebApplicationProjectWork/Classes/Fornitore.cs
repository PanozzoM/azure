﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProjectWork.Classes
{
    public class Fornitore
    {
        public Int32 FornitoreID { get; set; }
        public String Nome { get; set; }
    }
}