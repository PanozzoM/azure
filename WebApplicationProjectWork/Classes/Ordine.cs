﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProjectWork.Classes
{
    public class Ordine
    {
        public Int32 AmazonOrderId { get; set; }  
        public String BuyerEmail { get; set; }
        public String BuyerName { get; set; }
        public String CompanyLegalName { get; set; }
        public DateTime EarliestShipDate { get; set; }
        public String FulfillmentChannel { get; set; }
        public Boolean IsBusinessOrder { get; set; }
        public Boolean IsGlobalExpressEnabled { get; set; }
        public Boolean IsPremiumOrder { get; set; }
        public Boolean IsPrime { get; set; }
        public Boolean IsSoldByAB { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime LatestShipDate { get; set; }
        public String MarketplaceId { get; set; }
        public Int32 NumberOfItemsShipped { get; set; }
        public Int32 NumberOfItemsUnshipped { get; set; }
        public String OrderStatus { get; set; }
        public String OrderType { get; set; }
        public String PaymentMethod { get; set; }
        public String PaymentMethodDetails { get; set; }
        public DateTime PurchaseDate { get; set; }
        public String PurchaseOrderNumber { get; set; }
        public String ShipmentServiceLevelCategory { get; set; }
        public String ShippingAddressCity { get; set; }
        public String ShippingAddressLine1 { get; set; }
        public String ShippingAddressName { get; set; }
        public String ShippingCityStateOrRegion { get; set; }
        public String ShippingStateOrRegionPostalCode { get; set; }
    }

    public class Ordini
    {
        public List<Ordine> ordini { get; set; }
    }
}