﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace WebApplicationProjectWork.Classes
{
    public class Articolo
    {
        public Int32 ItemID { get; set; }
        public String ASIN { get; set; }
        public String Title { get; set; }
        public String Categoria { get; set; }
        public Double Prezzo { get; set; }
        public Int32 Giacenza { get; set; }
        public String Brand { get; set; }

    }
}