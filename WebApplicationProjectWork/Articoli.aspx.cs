﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProjectWork.Classes;

namespace WebApplicationProjectWork
{
    public partial class Articoli : System.Web.UI.Page
    {

        private String miaPassword = "****";










































        protected void Page_Load(object sender, EventArgs e)
        {
            getAll();
            this.Aggiungi.Click += Aggiungi_Click;
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                mySqlConnection.Open();
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text;

                mySqlCommand.Parameters.Add("@ASIN", SqlDbType.NVarChar);
                mySqlCommand.Parameters["@ASIN"].Value = ASIN.Text;
                mySqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar);
                mySqlCommand.Parameters["@Title"].Value = title.Text;
                mySqlCommand.Parameters.Add("@Categoria", SqlDbType.NVarChar);
                mySqlCommand.Parameters["@Categoria"].Value = Categoria.Text;
                mySqlCommand.Parameters.Add("@Prezzo", SqlDbType.Decimal);
                mySqlCommand.Parameters["@Prezzo"].Value = Prezzo.Text;
                mySqlCommand.Parameters.Add("@Giacenza", SqlDbType.Int);
                mySqlCommand.Parameters["@Giacenza"].Value = Giacenza.Text;
                mySqlCommand.Parameters.Add("@Brand", SqlDbType.NVarChar);
                mySqlCommand.Parameters["@Brand"].Value = Brand.Text;

                mySqlCommand.CommandText = "INSERT INTO Items(ASIN, Title, Categoria, Prezzo, Giacenza, Brand) " +
                                           "VALUES (@ASIN, @Title, @Categoria, @Prezzo, @Giacenza, @Brand)";
                mySqlCommand.ExecuteNonQuery();
                this.MessaggioInsert.Text = "Articolo Inserito";
            }
            catch (Exception exc)
            {
                this.MessaggioInsert.Text = "errore nell'inserimento dell'articolo: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        private void getAll()
        {
            SqlConnection mySqlConnection = null;
            try
            {
                String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                mySqlConnection = new SqlConnection(stringaConnessione);
                SqlCommand mySqlCommand = new SqlCommand();
                mySqlCommand.Connection = mySqlConnection;
                mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                mySqlCommand.CommandText = "SELECT * FROM Items";
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                if (mySqlDataReader.Read())
                {
                    List<Articolo> myListArticoli = new List<Articolo>();
                    while (mySqlDataReader.Read())
                    {
                        Articolo myArticolo = new Articolo()
                        {
                            ItemID = Convert.ToInt32(mySqlDataReader["ItemID"]),
                            ASIN = Convert.ToString(mySqlDataReader["ASIN"]),
                            Title = Convert.ToString(mySqlDataReader["Title"]),
                            Categoria = Convert.ToString(mySqlDataReader["Categoria"]),
                            Prezzo = Convert.ToDouble(mySqlDataReader["Prezzo"]),
                            Giacenza = Convert.ToInt32(mySqlDataReader["Giacenza"]),
                            Brand = Convert.ToString(mySqlDataReader["Brand"])
                        };
                        myListArticoli.Add(myArticolo);
                    }

                    GridView.DataSource = myListArticoli;
                    GridView.DataBind();
                }
                else
                {
                    Messaggio.Text = "Non sono presenti articoli";
                }
            }
            catch (Exception exc)
            {
                Messaggio.ForeColor = Color.Red;
                Messaggio.Text = "Non è stato posibile caricare gli articoli a causa di un errore: " + exc.Message;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }
    }
}