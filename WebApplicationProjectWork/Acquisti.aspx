﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Acquisti.aspx.cs" Inherits="WebApplicationProjectWork.Acquisti" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Acquisti</h1>
    <br />
    <asp:Label ID="Messaggio" runat="server" Text=""></asp:Label>
    <asp:GridView ID="GridView" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="696px">
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <br />
    <h2>Articoli</h2>
    <asp:GridView ID="GridViewArticoli" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="1280px">
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <br />
    <h2>Carrello</h2>
    <asp:GridView ID="GridViewCarrello" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="1280px">
        <AlternatingRowStyle BackColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <br />
    <table style="width: 30%;">
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelASIN" runat="server" Text="Fornitore: "></asp:Label></td>
            <td>
                <asp:DropDownList ID="listaFornitori" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelTitle" runat="server" Text="Data fattura: "></asp:Label></td>
            <td><asp:TextBox ID="dataFattura" runat="server" class="form-control" TextMode="Date"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 129px"><asp:Label ID="LabelCategoria" runat="server" Text="Numero Fattura: "></asp:Label></td>
            <td><asp:TextBox ID="numeroFattura" runat="server" class="form-control"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2"><asp:Button ID="Aggiungi" runat="server" Text="Aggiungi" class="btn btn-primary" Width="129px"/></td>
        </tr>
    </table>
    <asp:Label ID="MessaggioInsert" runat="server" Text=""></asp:Label>
</asp:Content>
