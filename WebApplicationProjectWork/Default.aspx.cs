﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplicationProjectWork
{
    public partial class _Default : Page
    {

        private String miaPassword = "****";



































        protected void Page_Load(object sender, EventArgs e)
        {
            this.Login.Click += Login_Click;
        }

        private void Login_Click(object sender, EventArgs e)
        {
            if (Email.Text != "" && Password.Text != "")
            {
                SqlConnection mySqlConnection = null;
                try
                {
                    String stringaConnessione = "Server=tcp:server-marco-p.database.windows.net,1433;Initial Catalog=MioDatabase;Persist Security Info=False;User ID=marco;Password=" + miaPassword + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    mySqlConnection = new SqlConnection(stringaConnessione);
                    SqlCommand mySqlCommand = new SqlCommand();
                    mySqlCommand.Connection = mySqlConnection;
                    mySqlCommand.CommandType = System.Data.CommandType.Text; // facoltativa
                    mySqlCommand.CommandText = "SELECT * FROM Users WHERE Email='" + Email.Text + "' AND Password='" + Password.Text + "'";
                    mySqlConnection.Open();
                    SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                    if (mySqlDataReader.Read())
                    {
                        Messaggio.ForeColor = Color.Green;
                        Messaggio.Text = "Login effettuato";
                    }
                    else
                    {
                        Messaggio.ForeColor = Color.Red;
                        Messaggio.Text = "Credenziali errate";
                    }
                }
                catch (Exception exc)
                {
                    Messaggio.ForeColor = Color.Red;
                    Messaggio.Text = "Errore: " + exc.Message;
                }
                finally
                {
                    mySqlConnection.Close();
                }
            }
            else
            {
                Messaggio.ForeColor = Color.Red;
                Messaggio.Text = "Email e/o Password mancanti";
            }
        }
    }
}