﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationProjectWork._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>LOGIN</h2>
                <p>
                    <asp:Label ID="LabelEmail" runat="server" Text="Inserisci email: "></asp:Label>
                    <br />
                    <asp:TextBox ID="Email" runat="server" class="form-control"></asp:TextBox>
                    <br />
                    <asp:Label ID="LabelPassword" runat="server" Text="Inserisci password: "></asp:Label>
                    <br />
                    <asp:TextBox ID="Password" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                    <br />
                    <asp:Label ID="Messaggio" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:Button ID="Login" runat="server" Text="Login" class="btn btn-primary"/>
                </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
